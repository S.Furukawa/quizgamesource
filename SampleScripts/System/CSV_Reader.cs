﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CSV_Reader
{
    public List<string[]> ReadCSV(List<string[]> csvData)
    {
        TextAsset csvFile = Resources.Load("QuizData/QuestionData") as TextAsset;

        StringReader reader = new StringReader(csvFile.text);

        while (reader.Peek() != -1)
        {
            string line = reader.ReadLine();
            csvData.Add(line.Split(','));
        }

        return csvData;
    }
}
