﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuizData
{
    [System.NonSerialized] readonly string categoryName;
    [SerializeField] string mainQuestion, commentary, correctAnswer;
    [SerializeField] List<string> answer;

    public string CategoryName { get => categoryName; }
    public string MainQuestion { get => mainQuestion; }
    public string Commentary { get => commentary; }
    public string CorrectAnswer { get => correctAnswer; }
    public List<string> Answer { get => answer; }

    public QuizData(string categoryName, string mainQuestion, string commentary, string correctAnswer)
    {
        this.categoryName = categoryName;
        this.mainQuestion = mainQuestion;
        this.commentary = commentary;
        this.correctAnswer = correctAnswer;
        answer = new List<string>();
    }
}
