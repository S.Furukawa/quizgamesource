﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuizDataList
{
    [SerializeField] string categoryName;
    [SerializeField] List<QuizData> quizDatas = new List<QuizData>();

    public List<QuizData> QuizDatas { get => quizDatas; }
    public string CategoryName { get => categoryName; set => categoryName = value; }
}
