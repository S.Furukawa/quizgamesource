﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum Game_State
    {
        Title,
        ModeSelect,
        PracticeMode,
        ExamMode,
        Questions,
        Result
    }

    public enum Mode_State
    {
        Empty,
        Practice_Mode,
        Exam_Mode
    }

    [SerializeField]
    private Game_State gameState;
    [SerializeField]
    private Mode_State modeState;

    [SerializeField]
    private MasterDataList masterData;

    [SerializeField]
    private GameUI gameUI;

    [SerializeField]
    private TitleController titleController;
    [SerializeField]
    private ModeSelectController modeSelectController;
    [SerializeField]
    private PracticeModeController practiceMode;
    [SerializeField]
    private ExamModeController examMode;
    [SerializeField]
    private QuestionsController questions;
    [SerializeField]
    private ResultController resultController;
    


    public Game_State GameState { get => gameState; set => gameState = value; }
    public GameUI GameUI { get => gameUI; }
    public TitleController TitleController { get => titleController; }
    public ModeSelectController ModeSelectController { get => modeSelectController; }
    public MasterDataList MasterData { get => masterData; }

    // ゲーム起動時の設定
    public void Start()
    {
        masterData.SetQuizData();
        ToTitle();
    }

    // タイトル画面の設定
    public void ToTitle()
    {
        gameState = Game_State.Title;
        titleController.SetTitle(gameUI.CreatePanel(gameState));
    }

    // モード選択へ移行する処理
    public void ToModeSelect()
    {
        gameState = Game_State.ModeSelect;
        modeState = Mode_State.Empty;
        modeSelectController.SetModeSelect(gameUI.CreatePanel(gameState));
    }

    // PracticeModeへ移行する処理
    public void ToPracticeMode()
    {
        gameState = Game_State.PracticeMode;
        modeState = Mode_State.Practice_Mode;
        practiceMode.SetPracticeMode(gameUI.CreatePanel(gameState));
    }

    // ExamModeへ移行する処理
    public void ToExamMode()
    {
        gameState = Game_State.ExamMode;
        modeState = Mode_State.Exam_Mode;
        examMode.SetExamMode(gameUI.CreatePanel(gameState));
    }


    // ExamModeからQuestionsへ移行する処理　＊override
    public void ToQuestions() { }

    // PracticeModeからQuestionsへ移行する処理　＊override
    public void ToQuestions(string selectCategory, int questionCount)
    {
        gameState = Game_State.Questions;

        // Questionsに問題も同時渡すようにする
        questions.SetQuestions(
            questionsPanel: gameUI.CreatePanel(gameState),
            questions: CreateQuestions(selectCategory, questionCount),
            modeState: modeState);
    }

    // Questionsに渡す問題を用意する
    public QuizData[] CreateQuestions(string category, int count)
    {
        QuizData[] quizDatas = new QuizData[count];
        QuizDataList quizDataList = new QuizDataList();

        // MasterDataから同じカテゴリーのリストを取得
        foreach (QuizDataList data in masterData.DataList)
            if (data.CategoryName == category)
                { quizDataList = data; break; }

        // 問題数から出題数分ランダムに数値を出す
        List<int> randomNum = new List<int>();

        while (randomNum.Count < count)
        {
            int random = Random.Range(0, quizDataList.QuizDatas.Count);

            if (randomNum.Count == 0) { randomNum.Add(random); }
            if (!randomNum.Contains(random)) { randomNum.Add(random); }
        }

        // 取得したリストから count 分の問題を追加
        for (int i = 0; i < count; i++)
            quizDatas[i] = quizDataList.QuizDatas[randomNum[i]];

        return quizDatas;
    }

    // リザルト画面に移行する処理
    public void ToResult()
    {
        gameState = Game_State.Result;
        resultController.SetResult(gameUI.CreatePanel(gameState));
    }
}
