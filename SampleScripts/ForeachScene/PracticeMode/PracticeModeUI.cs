﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PracticeModeUI : MonoBehaviour
{
    [SerializeField]
    private PracticeModeController practiceModeController;
    public Button toModeSelectButton, toQuestionsButton, menuButton;
    public Transform categoryMenuField;
    public Dropdown questionCountDropdown;
    

    public void SetPracticeModeUI(PracticeModeController practiceModeController, int categoryCount)
    {
        this.practiceModeController = practiceModeController;
        practiceModeController.QuestionCount = 3;
        CreateCategoryButton(categoryCount);
        SetButtonEvent();
    }

    // 問題数をセット
    public void SetQuestionCount()
    {
        int count = 0;
        switch (questionCountDropdown.value)
        {
            case 0: count = 3; break;
            case 1: count = 5; break;
            case 2: count = 10; break;
        }
        practiceModeController.QuestionCount = count;
    }

    // カテゴリーボタンを生成
    public void CreateCategoryButton(int categoryCount)
    {
        for (int i = 0; i < categoryCount; i++)
        {
            var count = i;
            // ボタンを生成
            var button = Instantiate(menuButton, categoryMenuField);

            // ボタンのテキストにカテゴリー名を追加
            button.transform.GetChild(0).GetComponent<Text>().text =
                practiceModeController.GetCategoryText(count);

            // ボタンにイベントをセット
            button.onClick.AddListener(
                () => practiceModeController.SetSelectCategory(count));
        }
    }


    public void SetButtonEvent()
    {
        toModeSelectButton.onClick.AddListener(
            () => practiceModeController.OnClickToModeSelect());

        toQuestionsButton.onClick.AddListener(
            () => practiceModeController.OnClickToQuestions());
    }
}
