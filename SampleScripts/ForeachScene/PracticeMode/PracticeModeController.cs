﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PracticeModeController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private PracticeModeUI practiceModeUI;

    [SerializeField] string selectCategory;
    [SerializeField] int questionCount;

    public string SelectCategory { get => selectCategory; set => selectCategory = value; }
    public int QuestionCount { get => questionCount; set => questionCount = value; }

    public void SetPracticeMode(GameObject practiceModePanel)
    {
        practiceModeUI = practiceModePanel.GetComponent<PracticeModeUI>();
        practiceModeUI.SetPracticeModeUI(this, gameManager.MasterData.DataList.Count);
    }

    public string GetCategoryText(int category)
    {
        return gameManager.MasterData.DataList[category].CategoryName;
    }

    public void SetSelectCategory(int selectCategory)
    {
        this.selectCategory = gameManager.MasterData.DataList[selectCategory].CategoryName;
    }

    public void OnClickToModeSelect()
    {
        Debug.Log("モード選択へ移行");
        selectCategory = ""; // カテゴリーをリセットする
        gameManager.ToModeSelect();
    }

    // クイズをスタートさせるメソッド
    public void OnClickToQuestions()
    {
        if (selectCategory.Length == 0 || selectCategory == "") return;
        Debug.Log("問題スタート");

        gameManager.ToQuestions(
            selectCategory: selectCategory,
            questionCount: questionCount);
    }
}
