﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleUI : MonoBehaviour
{
    [SerializeField]
    private TitleController titleController;

    public Button toModeSelectButton;

    public void SetTitleUI(TitleController titleController)
    {
        this.titleController = titleController;
        SetButtonEvent();
    }

    public void SetButtonEvent()
    {
        toModeSelectButton.onClick.AddListener(() => titleController.OnClickToModeSelect());
    }
}
