﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private TitleUI titleUI;

    public void SetTitle(GameObject titlePanel)
    {
        titleUI = titlePanel.GetComponent<TitleUI>();
        titleUI.SetTitleUI(this);
    }

    public void OnClickToModeSelect()
    {
        Debug.Log("モード選択へ移行");
        gameManager.ToModeSelect();
    }
}
