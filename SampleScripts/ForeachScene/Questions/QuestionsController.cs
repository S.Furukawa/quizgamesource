﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionsController : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] GameManager.Mode_State modeState;

    private QuestionsUI questionsUI;

    [SerializeField] QuizData[] currentQuizList;
    [SerializeField] HoldTheAnswer[] holdTheAnswers;

    public void SetQuestions(GameObject questionsPanel,
        QuizData[] questions, GameManager.Mode_State modeState)
    {
        this.modeState = modeState;
        currentQuizList = questions;

        holdTheAnswers = new HoldTheAnswer[currentQuizList.Length];
        for (int i = 0; i < currentQuizList.Length; i++)
            holdTheAnswers[i] = new HoldTheAnswer();
        
        questionsUI = questionsPanel.GetComponent<QuestionsUI>();
        questionsUI.SetQuestionsUI(this);
    }

    public void OnClickToTitle()
    {
        Debug.Log("タイトルへ移行");
        currentQuizList = null;
        gameManager.ToTitle();
    }

    public void OnClickToResult()
    {
        Debug.Log("TEST: リザルト画面に移行");
        gameManager.ToResult();
    }
}
