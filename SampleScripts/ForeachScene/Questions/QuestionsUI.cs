﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionsUI : MonoBehaviour
{
    [SerializeField]
    private QuestionsController questionsController;
    public Text selectModeText, categoryNameText, quesionCountText;
    public Text mainQuestionTextPrefab;
    public Button toTitleButton, toNextButton;
    public Button answerButtonPrefab;

    public void SetQuestionsUI(QuestionsController questionsController)
    {
        this.questionsController = questionsController;
        SetButtonEvent();
    }

    public void SetButtonEvent()
    {
        toTitleButton.onClick.AddListener(
            () => questionsController.OnClickToTitle());

        toNextButton.onClick.AddListener(
            () => Debug.Log("次の問題へ"));
    }
}
