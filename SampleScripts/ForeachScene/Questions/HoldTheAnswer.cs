﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HoldTheAnswer
{
    [SerializeField] string answer;
    [SerializeField] bool Judge;

    public string Answer { get => answer; set => answer = value; }
    public bool Judge1 { get => Judge; set => Judge = value; }
}
