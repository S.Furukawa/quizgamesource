﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultUI : MonoBehaviour
{
    [SerializeField]
    private ResultController resultController;
    public Button toModeSelectButton, toTitleButton;

    public void SetResultUI(ResultController resultController)
    {
        this.resultController = resultController;
        SetButtonEvent();
    }

    public void SetButtonEvent()
    {
        toModeSelectButton.onClick.AddListener(
            () => resultController.OnClickToModeSelect());

        toTitleButton.onClick.AddListener(
            () => resultController.OnClickToTitle());
    }
}
