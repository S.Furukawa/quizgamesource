﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private ResultUI resultUI;

    public void SetResult(GameObject resultPanel)
    {
        resultUI = resultPanel.GetComponent<ResultUI>();
        resultUI.SetResultUI(this);
    }

    public void OnClickToModeSelect()
    {
        Debug.Log("モード選択へ移行");
        gameManager.ToModeSelect();
    }

    public void OnClickToTitle()
    {
        Debug.Log("タイトルへ移行");
        gameManager.ToTitle();
    }

}
