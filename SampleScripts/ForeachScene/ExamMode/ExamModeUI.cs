﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExamModeUI : MonoBehaviour
{
    [SerializeField]
    private ExamModeController examModeController;
    public Button toModeSelectButton, toQuestionsButton;

    public void SetExamModeUI(ExamModeController examModeController)
    {
        this.examModeController = examModeController;
        SetButtonEvent();
    }

    public void SetButtonEvent()
    {
        toModeSelectButton.onClick.AddListener(
            () => examModeController.OnClickToModeSelect());

        toQuestionsButton.onClick.AddListener(
            () => examModeController.OnClickToQuestions());
    }
}
