﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExamModeController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private ExamModeUI examModeUI;

    public void SetExamMode(GameObject examModePanel)
    {
        examModeUI = examModePanel.GetComponent<ExamModeUI>();
        examModeUI.SetExamModeUI(this);
    }

    public void OnClickToModeSelect()
    {
        Debug.Log("モード選択へ移行");
        gameManager.ToModeSelect();
    }

    public void OnClickToQuestions()
    {
        Debug.Log("問題スタート");
        gameManager.ToQuestions();
    }
}
