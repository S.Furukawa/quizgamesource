﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeSelectUI : MonoBehaviour
{
    [SerializeField]
    private ModeSelectController modeSelectController;
    public Button toTitleButton, pracModeButton, examModeButton;

    public void SetModeSelectUI(ModeSelectController modeSelectController)
    {
        this.modeSelectController = modeSelectController;
        SetButtonEvent();
    }

    public void SetButtonEvent()
    {
        Button[] buttons = { toTitleButton, pracModeButton, examModeButton };
        for (int i = 0; i < buttons.Length; i++)
        {
            var count = i;
            buttons[i].onClick.AddListener(
                () => modeSelectController.OnClickToSelect(count));
        }
    }
}
