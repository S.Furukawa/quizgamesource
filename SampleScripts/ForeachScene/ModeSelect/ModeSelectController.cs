﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeSelectController : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private ModeSelectUI modeSelectUI;

    public const int TITLE = 0, PRACTICE = 1, EXAM = 2;

    public void SetModeSelect(GameObject modeSelectPanel)
    {
        modeSelectUI = modeSelectPanel.GetComponent<ModeSelectUI>();
        modeSelectUI.SetModeSelectUI(this);
    }

    public void OnClickToSelect(int selectMode)
    {
        switch (selectMode)
        {
            case TITLE:
                Debug.Log("タイトルへ戻る");
                gameManager.ToTitle();
                break;
            case PRACTICE:
                Debug.Log("PracticeModeへ移行");
                gameManager.ToPracticeMode();
                break;
            case EXAM:
                Debug.Log("ExamModeへ移行");
                gameManager.ToExamMode();
                break;
        }

    }
}
