﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterDataList : MonoBehaviour
{
    [SerializeField] List<QuizDataList> dataList = new List<QuizDataList>();

    public List<QuizDataList> DataList { get => dataList; }


    public void SetQuizData()
    {
        List<string[]> data = new List<string[]>();
        CSV_Reader reader = new CSV_Reader();
        reader.ReadCSV(data);

        // カテゴリー判別用カウント
        int count = 0;
        
        dataList.Add(new QuizDataList());

        for (int i = 0; i < data.Count; i++)
        {
            QuizData quizData = new QuizData(data[i][0], data[i][1], data[i][2], data[i][3]);

            for (int j = 4; j < data[i].Length; j++) quizData.Answer.Add(data[i][j]);


            if (i == 0)
            {
                dataList[count].QuizDatas.Add(quizData);
                dataList[count].CategoryName = dataList[count].QuizDatas[0].CategoryName;
            } 
            else
            {
                if (dataList[count].QuizDatas[0].CategoryName == quizData.CategoryName)
                {
                    dataList[count].QuizDatas.Add(quizData);
                }
                else
                {
                    dataList.Add(new QuizDataList());
                    count++;

                    dataList[count].QuizDatas.Add(quizData);
                    dataList[count].CategoryName = dataList[count].QuizDatas[0].CategoryName;
                }
            }
        }
    }
}
