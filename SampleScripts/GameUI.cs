﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public Transform mainPanelField;

    public GameObject titlePanel, modeSelectPanel,
        practiceModePanel, examModePanel, questionsPanel, resultPanel;


    public GameObject CreatePanel(GameManager.Game_State state)
    {
        // MainPanelFieldをリセット
        if (mainPanelField.childCount != 0)
            { foreach (Transform panel in mainPanelField) Destroy(panel.gameObject); }

        // 空のGameObjectの入れ物を用意
        GameObject scenePanel = null;

        switch (state)
        {
            case GameManager.Game_State.Title:
                scenePanel = Instantiate(titlePanel, mainPanelField);
                break;
            case GameManager.Game_State.ModeSelect:
                scenePanel = Instantiate(modeSelectPanel, mainPanelField);
                break;
            case GameManager.Game_State.PracticeMode:
                scenePanel = Instantiate(practiceModePanel, mainPanelField);
                break;
            case GameManager.Game_State.ExamMode:
                scenePanel = Instantiate(examModePanel, mainPanelField);
                break;
            case GameManager.Game_State.Questions:
                scenePanel = Instantiate(questionsPanel, mainPanelField);
                break;
            case GameManager.Game_State.Result:
                scenePanel = Instantiate(resultPanel, mainPanelField);
                break;
        }

        return scenePanel;
    }
}
